# Modular Angular Framework

## @hack/core

`@hack/core` is the framework's platform or scaffolding app. It bundles all dependencies and serves and Angular app that lazy loads remote modules. The remote modules are complete independent of the `@hack/core` and each other. They share the same underlying dependencies via `@hack/common`.

## Advantages

- Faster page loading between apps (aka dashboard)
- Reduced bundle sizes
- Lazy loading apps (aka dashboard)
- More control of the page view
- Stronger dependency management
- Consistency

## Disadvantages

- Additional complexity
- SystemJS required
- Stronger dependency management

## Credits

- David Lynam's Rebelcon 2018 talk [Our Journey To Modular UI Development](http://rebelcon.io/talks/david-lynam-journey-to-modular-ui/#talk)
- [Here is what you need to know about dynamic components in Angular](https://blog.angularindepth.com/here-is-what-you-need-to-know-about-dynamic-components-in-angular-ac1e96167f9e)
