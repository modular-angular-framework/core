FROM node:8 as build

RUN apt-get update && apt-get install -y wget --no-install-recommends \
  && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
  && apt-get update \
  && apt-get install -yq google-chrome-stable --no-install-recommends \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get purge --auto-remove -y curl \
  && rm -rf /src/*.deb

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package.json /usr/src/app/package.json

ARG NPM_CONFIG_REGISTRY="http://verdaccio-verdaccio:4873"

RUN npm install --global @angular/cli
RUN yarn --no-lockfile --registry $NPM_CONFIG_REGISTRY

COPY . /usr/src/app

RUN yarn build

FROM nginx:alpine
COPY --from=build /usr/src/app/dist/ /usr/share/nginx/html
