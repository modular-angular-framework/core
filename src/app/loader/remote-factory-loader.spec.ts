import { TestBed, inject } from '@angular/core/testing';

import { RemoteFactoryLoader } from './remote-factory-loader';

describe('RemoteFactoryLoaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoteFactoryLoader]
    });
  });

  it('should be created', inject(
    [RemoteFactoryLoader],
    (service: RemoteFactoryLoader) => {
      expect(service).toBeTruthy();
    }
  ));
});
