import {
  Compiler,
  Injectable,
  NgModuleFactory,
  NgModuleFactoryLoader
} from '@angular/core';

declare const SystemJS;

@Injectable()
export class RemoteFactoryLoader implements NgModuleFactoryLoader {
  constructor(private compiler: Compiler) {}

  /**
   * Used to load ng module factories from remote server
   * @param bundleName name of the UMD bundle create by ng-packagr
   */
  load(bundleName: string): Promise<NgModuleFactory<any>> {
    const url = `http://127.0.0.1:8888/hack-${bundleName}.umd.min.js`;
    return SystemJS.import(url)
      .then(module => module.EntryModule)
      .then(module => checkIfNotEmty(module, url))
      .then(module => this.compiler.compileModuleAsync(module));
  }
}

function checkIfNotEmty(module, url) {
  if (!module) {
    throw new Error(`Cannot find EntryModule in ${url}`);
  }
  return module;
}
