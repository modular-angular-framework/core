import {
  Compiler,
  CompilerFactory,
  COMPILER_OPTIONS,
  NgModule,
  NgModuleFactoryLoader,
  ModuleWithProviders
} from '@angular/core';
import { JitCompilerFactory } from '@angular/platform-browser-dynamic';
import { RemoteFactoryLoader } from './remote-factory-loader';

export function createCompiler(cf: CompilerFactory) {
  return cf.createCompiler();
}

/**
 * Inject JIT compiler with custom NgModuleFactoryLoader to
 * load RouterModule-less remote modules (eg. navbar)
 */
@NgModule({
  providers: [
    {
      provide: COMPILER_OPTIONS,
      useValue: {},
      multi: true
    },
    {
      provide: CompilerFactory,
      useClass: JitCompilerFactory,
      deps: [COMPILER_OPTIONS]
    },
    {
      provide: Compiler,
      useFactory: createCompiler,
      deps: [CompilerFactory]
    }
  ]
})
export class LoaderModule {
  /**
   * Inject RemoteFactoryLoader into root to ensure it is
   * available for app.component to load the navbar module.
   */
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LoaderModule,
      providers: [
        {
          provide: NgModuleFactoryLoader,
          useClass: RemoteFactoryLoader,
          deps: [Compiler]
        }
      ]
    };
  }
}
