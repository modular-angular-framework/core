import {
  Component,
  Injector,
  NgModuleFactory,
  NgModuleFactoryLoader,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(
    private loader: NgModuleFactoryLoader,
    private injector: Injector
  ) {}

  @ViewChild('navbar', { read: ViewContainerRef })
  navbar: ViewContainerRef;

  ngOnInit() {
    // Use RemoteFactoryLoader to fetch and resolve navbar module
    this.loader
      .load('navbar')
      .then(factory => this.resolveComponentFactory(factory))
      .then(factory => this.navbar.createComponent(factory))
      .catch(err => console.error(err)); // TODO: handle error
  }

  private resolveComponentFactory(factory: NgModuleFactory<any>) {
    const moduleRef = factory.create(this.injector);
    const compRef = moduleRef.injector.get('Entry');
    const resolver = moduleRef.componentFactoryResolver;
    return resolver.resolveComponentFactory(compRef);
  }
}
