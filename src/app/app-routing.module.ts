import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

declare const SystemJS;

/**
 * Lazy load remote bundle (AOT compatible!)
 * @param bundleName
 */
export const loadRemoteChildren = bundleName =>
  SystemJS.import(`http://127.0.0.1:8888/hack-${bundleName}.umd.min.js`)
    .then(module => module.EntryModule)
    .catch(err => console.error(err)); // TODO: error handling

const routes: Routes = [
  {
    path: 'dashboard',
    children: [
      {
        path: 'alpha',
        loadChildren: () => loadRemoteChildren('dashboard-alpha')
      },
      {
        path: 'beta',
        loadChildren: () => loadRemoteChildren('dashboard-beta')
      }
    ]
  },
  { path: '', redirectTo: 'dashboard/alpha', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
