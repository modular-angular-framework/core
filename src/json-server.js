const { company, name, address, random } = require('faker');

module.exports = () => {
  const users = [];
  for (let i = 0; i < 10; i++) {
    users.push({
      name: `${name.firstName()} ${name.lastName()}`,
      country: address.country(),
      company: company.companyName()
    });
  }

  const items = [];
  for (let i = 0; i < 10; i++) {
    items.push({
      id: random.word(),
      value: random.number()
    });
  }

  return { users, items };
}